# 1. MongoDB connection details
MONGO_URI = 'mongodb://mongo:27017/evedemo'

# 2. Resource and items REST methods definition
RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PATCH', 'DELETE']

# 3. Data schema
people = {
    'schema': {
        'firstname': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 10,
        },
        'lastname': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 15,
            'required': True,
            'unique': True,
        }
     }
}

4. Endpoints definition
DOMAIN = {
    'people': people
}